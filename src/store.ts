import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    token:localStorage.getItem('xtoken') || ''
  },
  mutations: {
    setToken(state, tokenVal){
      state.token = tokenVal;
    }
  },
  actions: {

  },
  getters:{
    isLogin:state=> (!!state.token)
  }
})
