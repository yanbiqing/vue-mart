import axios, { AxiosRequestConfig, AxiosResponse } from "axios";
import store from "./store";
import router from "./router";
axios.interceptors.request.use(
  (config: AxiosRequestConfig): AxiosRequestConfig => {
    if (store.state.token) {
    //   config.headers.token = store.state.token;
    }
    return config;
  }
);
axios.interceptors.response.use(
  res => {
    // 重新登陆
    if (res.status === 200) {
      if (res.data.code == -1) {
       logoutHandler();
      }
    }
    return res;
  },
  err => {
    if (err.response.status === 401) {
      // 未授权
      logoutHandler();
    }
  }
);
function logoutHandler(){
 // 清楚各种token
 localStorage.setItem("xtoken", "");
 store.commit("setToken", "");
 // 跳转链接
 router.push({
   path: "/login",
   query: {
     redirect: router.currentRoute.path
   }
 });
}