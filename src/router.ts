import Vue from 'vue'
import Router from 'vue-router'
import Home from './views/Home.vue'
import Login from './views/Login.vue';
import store from './store';
import mm from './common/mm';
Vue.use(Router)

const router = new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/',
      name: 'home',
      component: Home,
      meta:{auth:true}

    },{
      path: '/login',
      name: 'login',
      component: Login
    },
  ]
});
router.beforeEach((to, from, next)=>{
  // 放过的path
  if(to.path==='/login'){
    next();
    return;
  }
  // 如果有认证标志则认证
  if(to.meta.auth){
    // 判断是否有token
    let token = store.state.token;
    if(token){
      next();
    }else{
      next({
        path:'/login',
        query:{
          redirect:to.path,
          r:Date.now()
        }
      });
    }
  }else{
    // 无认证标志放过
    next();
  }
  
});

export default router;
