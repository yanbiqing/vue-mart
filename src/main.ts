import Vue from 'vue'
import './cube-ui'
import App from './App.vue'
import router from './router'
import store from './store'
import axios from 'axios';
import mm from './common/mm';
import './http-interceptor';
Vue.config.productionTip = false
Vue.prototype.$http = axios;
Vue.prototype.$mm = mm;
new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
