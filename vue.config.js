module.exports = {
  css: {
    loaderOptions: {
      stylus: {
        'resolve url': true,
        'import': [
          './src/theme'
        ]
      }
    }
  },
  pluginOptions: {
    'cube-ui': {
      postCompile: true,
      theme: true
    }
  },
  // 模拟接口
  configureWebpack: {
    devServer: {
      before(app) {
        // 检查令牌
        app.use(function (req, res, next) {
            if (/^\/api/.test(req.path)) {
              if (req.headers.token) {
                next();
              } else {
                res.sendStatus(401); // 需要认证
              }
            } else {
              next();
            }

          }),

          app.get('/api/login', function (req, res) {
            console.log('query', req.query)
            const {
              username,
              password
            } = req.query;
            if (username === 'admin' && password === 'admin') {
              res.json({
                code: 0,
                token: 'isLogin',
                msg: '登录成功'
              })
            } else {
              res.json({
                code: 1,
                token: '',
                msg: '账号密码错误'
              })
            }

          }),
          app.get('/api/goods', function (req, res) {
            res.json({
              code: 1,
              token: '',
              msg: '收到'
            })
          }),
          app.get('/logout', function (req, res) {
            res.json({
              code: -1
            })
          })
      }
    }
  }
}